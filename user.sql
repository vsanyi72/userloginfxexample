-- create schema
create schema user;
use user;

-- user tábla
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45), 
  `first_name` varchar(45),
  `last_name` varchar(45),
  `middle_name` varchar(45),
  `password` varchar(150),
  `email` varchar(45),
  PRIMARY KEY (`user_id`)
);
--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `user_name`, `first_name`, `last_name`, `middle_name`, `password`, `email`) VALUES (1,'user1','Gipsz','Gizi',NULL,'105076cf4d2baa9af78df83270ebce883fa0de87081a05cac9363df0cd5db45a0fe87c5e4b0fba004322a537b2ecc576a1e1693b397d9171e7be7f1388a3a447','user1@pelda.hu'),(2,'user1','Gipsz','Gizi',NULL,'105076cf4d2baa9af78df83270ebce883fa0de87081a05cac9363df0cd5db45a0fe87c5e4b0fba004322a537b2ecc576a1e1693b397d9171e7be7f1388a3a447','user1@pelda.hu'),(3,'user2','Marton','Jakab',NULL,'754cb2d71e5c57c92ea9c1d5ed6302a9fc065a0b566b87d2d14c2e562eaf8812bc942dd0b37d14f52eae690cafa3f3f38b2d99b9e493a425f226a7ecf4eea07b','user2@pelda.hu'),(4,'user1','Gipsz','Gizi',NULL,'105076cf4d2baa9af78df83270ebce883fa0de87081a05cac9363df0cd5db45a0fe87c5e4b0fba004322a537b2ecc576a1e1693b397d9171e7be7f1388a3a447','user1@pelda.hu');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
