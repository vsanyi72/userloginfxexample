package application.dao;

import java.sql.SQLException;

public interface Dao<T> {
	T read(int id) throws SQLException;
	void create(T entity) throws SQLException;
	void update(T entity) throws SQLException;
	void delete(T entity) throws SQLException;
}
