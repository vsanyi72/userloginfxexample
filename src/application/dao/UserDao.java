package application.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import application.model.User;

public class UserDao implements Dao<User> {

	private Connection connection;
	
	private String read;
	private String create;
	private String update;
	private String delete;
	
	private String login;
	
	public UserDao(Connection connection) {
		super();
		this.connection = connection;
		
		this.read = "SELECT * FROM USER WHERE user_id = ?";
		this.create = "INSERT INTO USER (user_name, first_name, last_name, middle_name, password, email) VALUES(?, ?, ?, ?, ?, ?)";
		this.update = "UPDATE USER SET user_name=?, first_name=?, last_name=?, middle_name=?, password=?, email=? WHERE user_id=?";
		this.delete = "DELETE FROM USER WHERE user_id=?";
		
		this.login = "SELECT user_id FROM USER WHERE user_name=? AND password=?";
	}

	public int login(String username, String password) throws SQLException {
		
		int userId = -1;
		
		try(PreparedStatement pst = this.connection.prepareStatement(this.login)){
			pst.setString(1, username);
			pst.setString(2, password);
			
			ResultSet result = pst.executeQuery();
			
			
			while(result.next()) {
				userId = result.getInt("user_id");
			}
			
		} catch (Exception e) {
			throw new SQLException("Hiba a result set kiolvasásában!", e);
		}
		return userId;
	}
	
	@Override
	public User read(int id) throws SQLException {
		User a = null;
		
		try(PreparedStatement pst = this.connection.prepareStatement(this.read)){
			pst.setInt(1, id);
			
			ResultSet result = pst.executeQuery();
			
			while(result.next()) {
				a = new User(
						result.getInt("user_id"),
						result.getString("user_name"),
						result.getString("first_name"),
						result.getString("last_name"),
						result.getString("middle_name"),
						result.getString("password"),
						result.getString("email"));
			}
			
		} catch (Exception e) {
			throw new SQLException("Hiba a result set kiolvasásában!", e);
		}
		return a;
	}

	@Override
	public void create(User entity) throws SQLException {
		try(PreparedStatement pst = this.connection.prepareStatement(this.create, Statement.RETURN_GENERATED_KEYS)){
			pst.setString(1, entity.getUserName());
			pst.setString(2, entity.getFirstName());
			pst.setString(3, entity.getLastName());
			pst.setString(4, entity.getMiddleName());
			pst.setString(5, entity.getPassword());
			pst.setString(6, entity.getEmail());
			
			int affectedRow = pst.executeUpdate();
			if(affectedRow <= 0) {
				throw new SQLException("SQL Insert failed!"); 
			}
			
			ResultSet generatedKeys = pst.getGeneratedKeys();
			if(generatedKeys.next()) {
				entity.setUserId(generatedKeys.getInt(1));
			}
			
			
		} catch (Exception e) {
			throw new SQLException("Hiba az insert közbe!", e);
		}
		
	}

	@Override
	public void update(User entity) throws SQLException {
		try(PreparedStatement pst = this.connection.prepareStatement(this.update)){
			pst.setString(1, entity.getUserName());
			pst.setString(2, entity.getFirstName());
			pst.setString(3, entity.getLastName());
			pst.setString(4, entity.getMiddleName());
			pst.setString(5, entity.getPassword());
			pst.setString(6, entity.getEmail());
			pst.setInt(7, entity.getUserId());
			
			pst.executeUpdate();
			
			
		} catch (Exception e) {
			throw new SQLException("Hiba az update közbe!", e);
		}
		
	}

	@Override
	public void delete(User entity) throws SQLException {
		try(PreparedStatement pst = this.connection.prepareStatement(this.delete)){
			pst.setInt(1, entity.getUserId());

			pst.executeUpdate();
			
			
		} catch (Exception e) {
			throw new SQLException("Hiba a delete közbe!", e);
		}
		
	}
}
