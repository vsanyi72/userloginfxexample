package application;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import application.dao.UserDao;
import application.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;

public class SampleController 
{
	private Connection conn;
	private UserDao dao;
	private Alert alert;
	
	@FXML
	private TextField tfUsername;
	@FXML
	private PasswordField tfPassword;
	@FXML
	private Button btnLogin;
	
	@FXML
	public void initialize() {
		try {
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/user", "root", "1234");
			this.dao = new UserDao(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	String sha512Pass = getSHA512("titkosjelszó");
//	User user = new User("user1", "Gipsz", "Gizi", sha512Pass, "user1@pelda.hu");
	
	
	
	public void login() {
		String username = this.tfUsername.getText();
		String password = this.tfPassword.getText();
		//ellenőrzések végzése(üres, nem üres, megefel bizonyos kritésiumoknak, stb)
		
		try {
			int id = dao.login(tfUsername.getText(), getSHA512(password));
			if(id > 0) {
				User u = dao.read(id);
				this.alert = new Alert(AlertType.INFORMATION, "Sikeres belépés", ButtonType.OK);
				this.alert.setHeaderText(u.getEmail());
			} else {
				this.alert = new Alert(AlertType.ERROR, "Sikertelen belépés", ButtonType.OK);
			}
			
			alert.show();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getSHA512(String password) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
		//salt megnézése
		
		byte[] result = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
		
		StringBuilder sb = new StringBuilder();
        for(int i=0; i< result.length ;i++){
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
		
        return sb.toString();
	}
	
}
